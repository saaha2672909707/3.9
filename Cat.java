import java.lang.reflect.Field;

public class Cat {

    public static void main(String[] args) throws IllegalAccessException {
        Animal animal= new Animal();
        characteristicAnimal(animal,"Tom",8,"blue");
    }

    public static void characteristicAnimal(Animal animal, String name,int age,String colorEyes) throws IllegalAccessException {
        Class<? extends Animal> aClass =animal.getClass();
        for(Field field : aClass.getDeclaredFields()){
            if ("name".equals(field.getName())){
                field.set(animal,name);
                System.out.println(name);
            }
            if ("age".equals(field.getName())){
                field.setAccessible(true);
                field.set(animal,age);
                System.out.println(age);
            }
            if ("colorEyes".equals(field.getName())){
                field.setAccessible(true);
                field.set(animal,colorEyes);
                System.out.println(colorEyes);
            }
        }
    }
}
